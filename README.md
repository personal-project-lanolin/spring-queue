<!-- https://raw.githubusercontent.com/jam01/SRS-Template/master/template.md -->
# Software Requirements Specification

## Для Student Queue

Version 0.1

Подготовлен Киселевым Артемом

Университет ИТМО

11.2019

# Содержание

* [История изменений](#-история-изменений)
* 1 [Введение](#1-введение)
  * 1.1 [Цель](#11-цель)
  * 1.2 [Товар](#12-товар)
  * 1.3 [Определения, аббревиатуры и сокращения](#13-определения-сокращения-и-аббревиатуры)
  * 1.4 [Рекомендации](#14-рекомендации)
  * 1.5 [Обзор](#15-обзор)
* 2 [Общее описание](#2-общее-описание)
  * 2.1 [Перспектива продукта](#21-перспектива-продукта)
  * 2.2 [Функции продукта](#22-функции-продукта)
  * 2.3 [Ограничения продукта](#23-ограничения-продукта)
  * 2.4 [Характеристики пользователя](#24-характеристики-пользователя)
  * 2.5 [Допущения и зависимости](#25-допущения-и-зависимости)
  * 2.6 [Распределение требований](#26-распределение-требований)
* 3 [Требования](#3-требования)
  * 3.1 [Внешние интерфейсы](#31-внешние-интерфейсы)
    * 3.1.1 [Пользовательские интерфейсы](#311-пользовательские-интерфейсы)
    * 3.1.2 [Аппаратные интерфейсы](#312-аппаратные-интерфейсы)
    * 3.1.3 [Программные интерфейсы](#313-программные-интерфейсы)
  * 3.2 [Функциональность](#32-функциональность)
  * 3.3 [Качество обслуживания](#33-качество-обслуживания)
    * 3.3.1 [Производительность](#331-производительность)
    * 3.3.2 [Безопасность](#332-безопасность)
    * 3.3.3 [Надежность](#333-надежность)
    * 3.3.4 [Доступность](#334-доступность)
  * 3.4 [Соответствие](#34-соответствие)
  * 3.5 [Проектирование и внедрение](#35-проектирование-и-внедрение)
    * 3.5.1 [Установка](#351-установка)
    * 3.5.2 [Распространение](#352-распространение)
    * 3.5.3 [Эксплуатация](#353-эксплуатация)
    * 3.5.4 [Многократное использование](#354-многократное-использование)
    * 3.5.5 [Мобильность](#355-мобильность)
    * 3.5.6 [Стоимость](#356-стоимость)
    * 3.5.7 [Крайний срок](#357-крайний-срок)
    * 3.5.8 [Доказательство концепции](#358-доказательство-концепции)
* 4 [Проверка](#4-проверка)
* 5 [Приложения](#5-приложения)

## История изменений

| Name | Date    | Reason For Changes  | Version   |
| ---- | ------- | ------------------- | --------- |
|      |         |                     |           |

<!--
      * 3.1.1.1 [Usability](#-external-interfaces)
      * 3.1.1.2 [Convenience](#-external-interfaces)
      * 3.1.2.1 [Interoperability](#-external-interfaces)
      * 3.1.3.1 [Interoperability](#-external-interfaces) -->

## 1. Введение

### 1.1 Цель

Цель документа - дать подробное описание требований к приложению "Student Queue". Он также объяснит системные ограничения, интерфейс и взаимодействие с другими внешними приложениями. Этот документ в первую очередь предназначен для того, чтобы быть предложенным заказчику для его утверждения и ссылки на разработку первой версии системы для команды разработчиков

### 1.2 Товар

"Student Queue" - это веб-приложение. которое помогает студентам быть в очереди и соблюдать очередность чего-либо. Приложение будет бесплатным и размещаться в сети Интернет. Старосты учебных групп могут создавать очереди и назначать следящих за порядком и продвижением очереди. Кроме того, программное обеспечение требует подключения к Интернету. Вся системная информация хранится в базе данных, которая находится на веб-сервере.

### 1.3 Определения, сокращения и аббревиатуры

| Термин              | Определение                                                                                       |
|---------------------|---------------------------------------------------------------------------------------------------|
| Студент             | Студен, который взаимодействует с приложением                                                     |
| Админ/Администратор | Системный администратор, которому дано специальное разрешение на управление и управление системой |
| Группа              | Определенное число студентов, которые взаимодействую друг с другом                                |
| Очередь             | Определённый порядок в следовании или в движении чего-либо или кого-либо                          |
| Староста            | Студент, являющийся формальной главой группы, может полностью управлять очередями своей группы    |
| Модератор           | Студент, являющийся тем, кто может управлять очередями после старосты                             |
| Веб-портал          | Веб-приложение, которое представляет просмотр очереди                                             |

### 1.4 Рекомендации

<!-- Перечислите любые другие документы или веб-адреса, на которые ссылается этот SRS. Они могут включать руководства по стилю пользовательского интерфейса, контракты, стандарты, спецификации системных требований, документы прецедентов или документ видения и области применения. Предоставьте достаточно информации, чтобы читатель мог получить доступ к копии каждой ссылки, включая название, автора, номер версии, дату и источник или местоположение. -->

### 1.5 Обзор

Остальная часть настоящего документа включает три главы и приложения.

Во втором разделе представлен обзор функциональности системы и взаимодействия системы с другими системами. В этой главе также представлены различные типы заинтересованных сторон и их взаимодействие с системой. Кроме того, в главе также упоминаются системные ограничения и предположения о продукте.

В третьей главе приводится подробная спецификация требований и описание различных интерфейсов системы. Для более точного определения требований к различным аудиториям используются различные методы спецификации.

В четвертой главе речь идет о приоритизации требований. Он включает в себя мотивацию для выбранных методов приоритизации и обсуждает, почему не были выбраны другие альтернативы.

Приложения в конце документа включают все результаты определения приоритетов требований и план выпуска на их основе.

## 2. Общее Описание

В этом разделе будет представлен обзор всей системы. Система будет объяснена в ее контексте, чтобы показать, как система взаимодействует с другими системами и ввести основные функциональные возможности его. В нем также будет описано, какие заинтересованные стороны будут использовать систему и какие функциональные возможности доступны для каждого типа. Наконец, будут представлены ограничения и допущения для системы.

### 2.1 Перспектива продукта

Эта система будет состоять из двух частей: веб-приложения и веб-браузера. Веб-приложение буде использоваться для управлением очередей и системы в целом. Веб-браузер будет использоваться для отображения информации из веб-приложения.

Поскольку приложение ориентировано на данных, ему нужно будет где-то хранить данные. Для этого будет использоваться СУБД. Вся связь с базой данных будет осуществляться через Интернет.

![График работы](md/diag_1.svg)

### 2.2 Функции продукта

С помощью браузера, пользователь сможет просматривать, вставать, управлять очередями. Результат будет основан на том, какую очередь будет выбирать пользователь. Будет существовать множество очередей, и администратор и старосты смогут управлять параметрами для тех критериев, которые имеют это.

Результат выбора очереди будет отображаться в виде списка. Представление списка будет иметь один элемент списка для каждого студента в данной очереди и показывать положение в очереди, его имя.

### 2.3 Ограничения продукта

Подключение к Интернету также является ограничением для приложения. Поскольку приложение извлекает данные из базы данных через Интернет, крайне важно, чтобы для работы приложения было установлено подключение к интернету.

Веб-приложение будет ограничено емкостью базы данных. Поскольку база данных одна на приложение, она может быть вынуждена помещать входящие запросы в очередь и, следовательно, увеличивать время, необходимое для извлечения данных.

### 2.4 Характеристики пользователя

Существует четыре типа пользователей, которые взаимодействуют с системой: студенты, модераторы, старосты, администраторы. Каждый из этих четырех пользователей имеет различное использование системы, поэтому каждый из них имеет свои собственные требования.

Студенты могут использовать приложение для просмотра доступных им очередей, и управлять своим положением в очереди.

Модераторы буду использовать приложение так же как и студенты, а так же для управлением положением других студентов в очереди.

Старосты буду использовать приложение так же как и модератор, а так же у них будет доступен более широкий функционал управление очередями: создание новых, удаление старых, модифицирование существующий. А так же может управлять списком студентов в группе.

Администраторы взаимодействуют с приложением напрямую. Могут управлять всем приложением, всеми студентами, очередями, группами. Доступен функцционал добавления группы и добавления студента.

### 2.5 Допущения и зависимости

Веб-браузер должен иметь последнюю доступную версию и поддерживать определенный ряд функций.

### 2.6 Распределение требований

## 3. Требования

Этот раздел содержит все функциональные и качественные требования к системе. В нем дается подробное описание системы и всех ее особенностей.

### 3.1 Внешние интерфейсы

В этом разделе приводится подробное описание всех входов в систему и выходов из нее. Он также дает описание аппаратных, программных и коммуникационных интерфейсов и предоставляет основные прототипы пользовательского интерфейса.

#### 3.1.1 Пользовательские интерфейсы

<!-- Describe the logical characteristics of each interface between the software product and the users. This may include sample screen images, any GUI standards or product family style guides that are to be followed, screen layout constraints, standard buttons and functions (e.g., help) that will appear on every screen, keyboard shortcuts, error message display standards, and so on. Define the software components for which a user interface is needed. Details of the user interface design should be documented in a separate user interface specification. -->

#### 3.1.2 Аппаратные интерфейсы

<!-- Describe the logical and physical characteristics of each interface between the software product and the hardware components of the system. This may include the supported device types, the nature of the data and control interactions between the software and the hardware, and communication protocols to be used. -->

#### 3.1.3 Программные интерфейсы

<!-- Describe the connections between this product and other specific software components (name and version), including databases, operating systems, tools, libraries, and integrated commercial components. Identify the data items or messages coming into the system and going out and describe the purpose of each. Describe the services needed and the nature of communications. Refer to documents that describe detailed application programming interface protocols. Identify data that will be shared across software components. If the data sharing mechanism must be implemented in a specific way (for example, use of a global data area in a multitasking operating system), specify this as an implementation constraint. -->

### 3.2 Функциональность

### 3.3 Качество обслуживания

<!-- Quality requirements state additional, quality-related properties that the functional effects of the  software should have. -->

#### 3.3.1 Производительность

<!-- If there are performance requirements for the product under various circumstances, state them here and explain their rationale, to help the developers understand the intent and make suitable design choices. Specify the timing relationships for real time systems. Make such requirements as specific as possible. You may need to state performance requirements for individual functional requirements or features. -->

#### 3.3.2 Безопасность

<!-- Specify any requirements regarding security or privacy issues surrounding use of the product or protection of the data used or created by the product. Define any user identity authentication requirements. Refer to any external policies or regulations containing security issues that affect the product. Define any security or privacy certifications that must be satisfied. -->

#### 3.3.3 Надежность

<!-- Specify the factors required to establish the required reliability of the software system at time of delivery. -->

#### 3.3.4 Доступность

<!-- Specify the factors required to guarantee a defined availability level for the entire system such as checkpoint, recovery, and restart. -->

### 3.4 Соответствие

<!-- Specify the requirements derived from existing standards or regulations, including:  
a) Report format;  
b) Data naming;  
c) Accounting procedures;  
d) Audit tracing.  

For example, this could specify the requirement for software to trace processing activity. Such traces are needed for some applications to meet minimum regulatory or financial standards. An audit trace requirement may, for example, state that all changes to a payroll database shall be recorded in a trace file with before and after values. -->

### 3.5 Проектирование и внедрение

#### 3.5.1 Установка

<!-- Constraints to ensure that the software-to-be will run smoothly on the target implementation platform. -->

#### 3.5.2 Распространение

<!-- Constraints on software components to fit the geographically distributed structure of the host organization, the distribution of data to be processed, or the distribution of devices to be controlled. -->

#### 3.5.3 Эксплуатация

<!-- Specify attributes of software that relate to the ease of maintenance of the software itself. These may include requirements for certain modularity, interfaces, or complexity limitation. Requirements should not be placed here just because they are thought to be good design practices. -->

#### 3.5.4 Многократное использование

#### 3.5.5 Мобильность

<!-- Specify attributes of software that relate to the ease of porting the software to other host machines and/or operating systems. -->

#### 3.5.6 Стоимость

#### 3.5.7 Крайний срок

#### 3.5.8 Доказательство концепции

## 4. Проверка

<!-- Provide the verification approaches and methods planned to qualify the software. The information items for verification are recommended to be given in a parallel manner with the information items in Section 3. -->

## 5. Приложения
