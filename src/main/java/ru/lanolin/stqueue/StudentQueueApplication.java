package ru.lanolin.stqueue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentQueueApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentQueueApplication.class, args);
	}

}
